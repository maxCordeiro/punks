﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuseMachineController : EntityController
{
    public bool displayOpen;
    public FuseMachineWindow display;

    public new void Start()
    {
        base.Start();

        entityType = EntityType.FUSE;
    }

    public new void Update()
    {
        base.Update();

        if (display == null) return;

        display.transform.position = transform.position + new Vector3(0, 2);
        display.transform.localPosition = new Vector3(Mathf.RoundToInt(display.transform.localPosition.x), Mathf.RoundToInt(display.transform.localPosition.y));
    }

    public void CloseDisplay()
    {
    }

    public override void OnDragEntity()
    {
        if (GameController.instance.grabbedEntityType != EntityType.PUNK || (GameController.instance.grabbedEntityType == EntityType.PUNK && GameController.instance.grabbedEntityController.GetPunkEntity().age == PunkAge.SPROUT))
        {
            GameController.instance.ReleaseGrabbedEntity(true);
            return;
        }

        if (GetFuseMachineEntity().AddPunk(GameVariables.punks[GameController.instance.indexOfPunk]))
        {
            GameVariables.punks.RemoveAt(GameController.instance.indexOfPunk);
            GameController.instance.UpdatePunkControllers();

            if (display != null)
            {
                display.UpdateDisplay();
            }

            return;
        }
        else
        {
            GameController.instance.ReleaseGrabbedEntity(true);
        }
    }

    public override void OnClick()
    {
        throw new System.NotImplementedException();
    }

    public override void OnClickUp()
    {
        GameController.instance.ClosePunkWindow();

        if (display != null)
        {
            GameObject.Destroy(display.gameObject);
            GameController.instance.ClosePunkWindow();
        }
        else
        {
            display = Instantiate<GameObject>(Resources.Load<GameObject>("FuseBoxWindow"), GameObject.FindGameObjectWithTag("GameCanvas").transform).GetComponent<FuseMachineWindow>();
            display.fuseController = this;
            display.UpdateDisplay();
        }
    }

    public override void OnItemLeftClick()
    {

    }

    public override void OnItemRightClick()
    {

    }
}
