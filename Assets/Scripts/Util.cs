﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

public static class Util
{
    public static T Choose<T>(T a, T b, params T[] p)
    {
        int random = Random.Range(0, p.Length + 2);
        if (random == 0) return a;
        if (random == 1) return b;
        return p[random - 2];
    }

    public static Vector3 ClampVector(Vector3 inVector, float x0, float y0, float x1, float y1, float z0)
    {
        inVector.x = Mathf.Clamp(inVector.x, x0, x1);
        inVector.y = Mathf.Clamp(inVector.y, y1, y0);
        inVector.z = z0;

        return inVector;
    }

    public static Vector3 RoundToGrid(Vector3 inVector)
    {
        inVector.x = Mathf.Round(inVector.x);// * 2) / 2;
        inVector.y = Mathf.Round(inVector.y);// * 2) / 2;

        return inVector;
    }

}
