﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ActivityViewer : EditorWindow
{
    [MenuItem("Punks/ActivityView")]
    public static void ShowWindow()
    {
        GetWindow(typeof(ActivityViewer));
    }
    
}
