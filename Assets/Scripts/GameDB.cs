﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class GameDB
{
    public const int DEFAULTMAXSTACK= 99;
    public const int DEFAULTMAXDURABILITY = 10;

    public static Dictionary<string, Item> itemDB = new Dictionary<string, Item>();

    public static Dictionary<string, string> langItems = new Dictionary<string, string>()
    {
        {"muscleberry", "Muscle Berry" },
        {"charmberry", "Charm Berry" },
        {"smartberry", "Smart Berry" },
        {"dexberry", "Dex Berry" },
        {"luckberry", "Luck Berry" },

        {"hoe", "Hoe" },

        {"seed_muscleberry", "Muscle Berry Seeds" },
    };

    public static Color[] punkColors = new Color[]
    {
        new Color(0.9f, 0.27f, 0.22f),
        new Color(1f, 0.93f, 0.51f),
        new Color(0.31f, 0.64f, 0.72f),
    };

    public static Dictionary<ActivityType, string> langActivityTitles = new Dictionary<ActivityType, string>()
    {
        {ActivityType.BOULDERPUSH, "Boulder Push" },
        {ActivityType.CHARM, "Boulder Push" },
        {ActivityType.SPELLINGBEE, "Spelling Bee" },
        {ActivityType.RACE, "Race" },
        {ActivityType.SPELUNKING, "Spelunking" }
    };

    public static Dictionary<ActivityType, string> langActivityDesc = new Dictionary<ActivityType, string>()
    {
        {ActivityType.BOULDERPUSH, "Boulder Push" },
        {ActivityType.CHARM, "Boulder Push" },
        {ActivityType.SPELLINGBEE, "Spelling Bee" },
        {ActivityType.RACE, "Race" },
        {ActivityType.SPELUNKING, "Spelunking" }
    };

    public static Dictionary<object, string> langMisc = new Dictionary<object, string>()
    {
        {PunkAge.SPROUT, "Sprout" },
        {PunkAge.ACTIVE, "Active" },
        {PunkAge.ELDER, "Elder" },
        {PunkAge.DEAD, "Dead" }
    };

    public static void BuildItemDB()
    {
        AddItem("muscleberry", ItemType.STATBERRY, _internalVars: new object[] { 0, 5 });
        AddItem("charmberry", ItemType.STATBERRY, _internalVars: new object[] { 1, 5 });
        AddItem("smartberry", ItemType.STATBERRY, _internalVars: new object[] { 2, 5 });
        AddItem("dexberry", ItemType.STATBERRY, _internalVars: new object[] { 3, 5 });
        AddItem("luckberry", ItemType.STATBERRY, _internalVars: new object[] { 4, 5 });

        AddItem("strongmuscleberry", ItemType.STATBERRY, _internalVars: new object[] { 0, 15 });
        AddItem("strongcharmberry", ItemType.STATBERRY, _internalVars: new object[] { 1, 15 });
        AddItem("strongsmartberry", ItemType.STATBERRY, _internalVars: new object[] { 2, 15 });
        AddItem("strongdexberry", ItemType.STATBERRY, _internalVars: new object[] { 3, 15 });
        AddItem("strongluckberry", ItemType.STATBERRY, _internalVars: new object[] { 4, 15 });

        AddItem("hoe", ItemType.TOOL, DEFAULTMAXDURABILITY, 1);
        AddItem("wateringcan", ItemType.TOOL, DEFAULTMAXDURABILITY, 1);

        AddItem("seed_muscleberry", ItemType.SEED, _internalVars: "muscleberry");
    }
    
    static void AddItem(string _id, ItemType _type, int _maxDurability = 10, int _maxStack = 99, params object[] _internalVars)
    {
        Item addedItem = new Item(_id, _type);
        addedItem.durability(_maxDurability);
        addedItem.maxStack = _maxStack;
        addedItem.internalVars = _internalVars.ToArray();

        itemDB.Add(_id, addedItem);
    }
}