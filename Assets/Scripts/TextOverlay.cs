﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextOverlay : MonoBehaviour
{
    public Text nameText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame

    void Update()
    {
        nameText.rectTransform.SetPositionAndRotation(Camera.main.WorldToScreenPoint(transform.position), Quaternion.identity);
    }
}
