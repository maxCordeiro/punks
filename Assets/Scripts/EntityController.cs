﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EntityController : MonoBehaviour
{
    public Entity entity;
    public EntityType entityType;
    public bool canMove;
    public bool canDrag;

    public abstract void OnClick();
    public abstract void OnClickUp();

    public abstract void OnDragEntity();
    public abstract void OnItemLeftClick();
    public abstract void OnItemRightClick();

    public void Start()
    {
        //canMove = true;
        canDrag = true;
    }

    public void Update()
    {
        if (entity == null || !canMove) return;

        entity.pos = transform.position;
    }

    public void UpdateEntityPos()
    {
        transform.position = entity.pos;
    }

    public object CastEntity<T>()
    {
        object castEntity = null;

        try
        {
            castEntity = Convert.ChangeType(castEntity, typeof(T));
        } catch
        {
        }

        return castEntity;
    }
    
    public Punk GetPunkEntity()
    {
        return (Punk)entity;
    }

    public ActivityCannon GetActivityCannonEntity()
    {
        return (ActivityCannon)entity;
    }

    public FuseMachine GetFuseMachineEntity()
    {
        return (FuseMachine)entity;
    }

    public Crop GetCropEntity()
    {
        return (Crop)entity;
    }

    public void AssignEntity(Entity e)
    {
        entity = e;
        UpdateEntityPos();

        canMove = true;
    }
}
