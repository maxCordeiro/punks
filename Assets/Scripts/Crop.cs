﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[System.Serializable]
public class Crop : Entity
{
    public ItemStack cropItem;
    public Punk cropPunk;

    public CropType cropType;

    public float growthTime = -1;
    public bool doneGrowing;

    public Crop (ItemStack _cropItem, float _growthTime)
    {
        cropItem = _cropItem;
        growthTime = _growthTime;
        cropType = CropType.ITEM;
    }

    public Crop (Punk _cropPunk, float _growthTime)
    {
        cropPunk = _cropPunk;
        growthTime = _growthTime;
        cropType = CropType.PUNK;

        cropPunk.pos = pos;
    }

    public Crop()
    {
        cropType = CropType.NONE;
    }

    public bool AddItem(ItemStack _cropitem, float _growthTime)
    {
        if (cropType != CropType.NONE) return false;

        cropItem = _cropitem;

        cropType = CropType.ITEM;
        growthTime = _growthTime;

        entityName = GameDB.langItems[cropItem.item.id] + "<br>" + growthTime.ToString() + "m";

        return true;
    }

    public bool AddPunk(Punk _cropPunk, float _growthTime)
    {
        if (cropType != CropType.NONE) return false;

        cropPunk = _cropPunk;
        cropPunk.pos = pos;

        cropType = CropType.PUNK;
        growthTime = _growthTime;

        return true;
    }
}

public enum CropType
{
    ITEM,
    PUNK,
    NONE
}