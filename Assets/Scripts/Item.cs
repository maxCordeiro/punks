﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[System.Serializable]
public class Item
{
    public readonly string id;
    public string displayId => id;

    public ItemType type;
    public int maxDurability;
    public int maxStack;

    public object[] internalVars;

    public Item(string _id, ItemType _type)
    {
        id = _id;
        type = _type;
        maxDurability = 10;
        maxStack = 30;
    }

    public Item durability (int durability) { maxDurability = durability; return this; }
}

[System.Serializable]
public class ItemStack
{
    public Item item;
    public int quantity;
    public int currentDurability;

    object param;

    public ItemStack (Item _item, int _quantity = 1)
    {
        item = _item;
        quantity = _quantity;
        currentDurability = _item.maxDurability;
    }

    public ItemStack (string _itemId, int _quantity = 1)
    {
        item = GameDB.itemDB[_itemId];
        quantity = _quantity;
        currentDurability = GameDB.itemDB[_itemId].maxDurability;
    }
}

public enum ItemType
{
    STATBERRY,
    TOOL,
    SEED
}