﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorController : MonoBehaviour
{
    PunkController grabbedPunk;
    bool isGrabbing;

    public Text punkDisplay;
    bool updatedDisplay;

    Rewired.Player rePlayer;

    public GameObject statWindow;
    public SuperTextMesh punkName;
    public SuperTextMesh[] statLetters;
    public SuperTextMesh[] statNames;
    public SuperTextMesh[] statAmounts;
    
    // Start is called before the first frame update
    void Start()
    {
        rePlayer = Rewired.ReInput.players.GetPlayer(0);
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D _hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        /*if (isGrabbing)
        {
            Vector3 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            cursorPos.z = 0;
            grabbedPunk.transform.position = cursorPos + new Vector3(0, 1);
            grabbedPunk.transform.GetChild(0).position = grabbedPunk.transform.position - new Vector3(0, 1.18f);

            UpdatePunkDisplay(true, grabbedPunk.transform);

        } else
        {
            if (_hit)
            {
                UpdatePunkDisplay(_hit, _hit.collider.transform);
            } else
            {
                UpdatePunkDisplay(false, null);
            }
        }*/
        /*
        if (rePlayer.GetButtonDown("PrimaryClick"))
        {
            if (_hit)
            {
                PunkController p = _hit.collider.GetComponent<PunkController>();

                if (p)
                {

                    statLetters[0].text = p.punk.muscleGrade.ToString();
                    statLetters[1].text = p.punk.charmGrade.ToString();
                    statLetters[2].text = p.punk.smartGrade.ToString();
                    statLetters[3].text = p.punk.dexGrade.ToString();
                    statLetters[4].text = p.punk.luckGrade.ToString();

                    statAmounts[0].text = p.punk.muscle.ToString();
                    statAmounts[1].text = p.punk.charm.ToString();
                    statAmounts[2].text = p.punk.smart.ToString();
                    statAmounts[3].text = p.punk.dex.ToString();
                    statAmounts[4].text = p.punk.luck.ToString();
                    statAmounts[5].text = p.punk.stamina.ToString() + "/100";

                    punkName.text = p.punk.nickname;

                    statWindow.SetActive(true);
                }
            } else
            {
                statWindow.SetActive(false);
            }
        }

        if (rePlayer.GetButtonDown("PrimaryClick"))
        {
            // Drag
            
            /*
            if (isGrabbing)
            {
                Vector3 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                cursorPos.z = 0;
                grabbedPunk.transform.position = cursorPos;
                grabbedPunk.transform.GetChild(0).localPosition = new Vector3(0, -0.18f, 0);

                isGrabbing = false;
                grabbedPunk = null;
            } else
            {

                if (_hit.collider != null)
                {
                    isGrabbing = true;
                    grabbedPunk = _hit.collider.GetComponent<PunkController>();
                }
            }
        }*/
    }

    void UpdatePunkDisplay(bool active, Transform pos)
    {/*
        if (active)
        {
            if (!punkDisplay.gameObject.activeInHierarchy) punkDisplay.gameObject.SetActive(true);

            punkDisplay.rectTransform.position = pos.position;//Camera.main.WorldToScreenPoint(pos.position);

            if (!updatedDisplay)
            {
                Punk hoverPunk = pos.GetComponent<PunkController>().punk;

                punkDisplay.text = hoverPunk.nickname;
                updatedDisplay = true;
            }
        }
        else {
            updatedDisplay = false;
            punkDisplay.gameObject.SetActive(false);
        }*/
    }
}
