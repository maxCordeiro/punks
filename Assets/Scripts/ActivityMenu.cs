﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ActivityMenu : UISystem, ITimeUpdate
{
    public ActivityPostUpdater[] activityPosts;

    bool exitTransition;
    RectTransform rect;

    int selected;

    public SuperTextMesh title;
    public SuperTextMesh desc;
    public SuperTextMesh info;
    public SuperTextMesh buttonText;

    public Material activeButton, inactiveButton;

    public Button sendButton;

    CanvasGroup child;

    // Start is called before the first frame update
    void Start()
    {
        selected = -1;

        child = transform.GetChild(0).GetComponent<CanvasGroup>();
        child.alpha = 0;
        child.interactable = false;
        rect = GetComponent<RectTransform>();
        rect.offsetMax = new Vector2(0, -180);

        LeanTween.value(-180, 0, 0.45f).setEaseOutSine().setOnUpdate((float val) => { rect.offsetMax = new Vector2(0, val); }).setOnComplete(()=> { OnCompleteEnter(); });

        sendButton.onClick.AddListener(() => { Send(); });
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.instance.rePlayer.GetButtonDown("Action") && !exitTransition)
        {
            exitTransition = true;

            child.interactable = false;
            LeanTween.alphaCanvas(child, 0, 0.2f);
            LeanTween.value(0, -180, 0.45f).setDelay(0.2f).setEaseOutSine().setOnUpdate((float val) => { rect.offsetMax = new Vector2(0, val); }).setOnComplete(() => { OnCompleteExit(); });
        }
    }

    void OnCompleteEnter()
    {
        

        for (int i = 0; i < activityPosts.Length; i++)
        {
            int ii = i;
            activityPosts[i].UpdateDisplay(i);

            activityPosts[i].button.onClick.AddListener(() => { UpdateDisplay(ii); });
        }
        UpdateDisplay(-1);
        LeanTween.alphaCanvas(child, 1, 0.2f).setOnComplete(() => { child.interactable = true; });
    }
    
    void OnCompleteExit()
    {
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        GameController.instance.openMenus.RemoveAt(GameController.instance.openMenus.IndexOf(this));
        GameController.instance.prevState = GameController.instance.state;
        GameController.instance.state = GameState.GARDEN;
    }

    void Send()
    {
        PunkSystem.SendToActivity(GameController.instance.indexOfPunk, selected);

        for (int i = 0; i < activityPosts.Length; i++)
        {
            activityPosts[i].UpdateDisplay(i);
        }

        GameController.instance.ReleaseGrabbedEntity();
        GameController.instance.UpdatePunkControllers();
        UpdateDisplay(selected);
    }

    void UpdateDisplay(int _index)
    {
        if (_index == -1)
        {
            title.gameObject.SetActive(false);
            desc.gameObject.SetActive(false);
            info.gameObject.SetActive(false);
            sendButton.interactable = false;
            buttonText.textMaterial = inactiveButton;
            buttonText.Rebuild();
            return;
        } else
        {
            title.gameObject.SetActive(true);
            desc.gameObject.SetActive(true);
            info.gameObject.SetActive(true);
            sendButton.interactable = GameVariables.currentActivities[_index].isParticipating? false : true;
        }

        // Does not need to check if it's a Punk because that check is done prior to dropping
        sendButton.interactable = GameController.instance.grabbedEntityController != null;

        buttonText.textMaterial = sendButton.interactable ? activeButton : inactiveButton;
        buttonText.Rebuild(false);

        selected = _index;

        LeanTween.scale(activityPosts[_index].gameObject, new Vector3(1, 1, 1), 0.2f).setFrom(new Vector3(0.9f, 0.9f));
    
        title.text = GameDB.langActivityTitles[GameVariables.currentActivities[_index].type];
        desc.text = GameDB.langActivityDesc[GameVariables.currentActivities[_index].type];

        int recommended = 0;
        string stat = "";

        switch (GameVariables.currentActivities[_index].type)
        {
            case ActivityType.BOULDERPUSH:
                stat = "Muscle";
                break;
            case ActivityType.CHARM:
                stat = "Charm";
                break;
            case ActivityType.SPELLINGBEE:
                stat = "Smart";
                break;
            case ActivityType.RACE:
                stat = "Dex.";
                break;
            case ActivityType.SPELUNKING:
                stat = "Luck";
                break;
        }

        switch (GameVariables.currentActivities[_index].difficulty)
        {
            case ActivityDifficulty.EASY:
                recommended = 50;
                break;
            case ActivityDifficulty.NORMAL:
                recommended = 200;
                break;
            case ActivityDifficulty.TOUGH:
                recommended = 400;
                break;
            case ActivityDifficulty.CHALLENGING:
                recommended = 625;
                break;
            case ActivityDifficulty.ELITE:
                recommended = 800;
                break;
            default:
                break;
        }

        string infoOut = "<b>Recommended:</b>";
        infoOut += "<a=right>" + recommended.ToString() + " " + stat + "</a><br>";
        infoOut += "<b>Stamina:</b>";
        infoOut += "<a=right>" + ((int)GameVariables.currentActivities[_index].difficulty).ToString() + "</a><br>";
        infoOut += "<b>Time:</b>";
        infoOut += "<a=right>" + GameVariables.currentActivities[_index].participatingTime.ToString() + "m" + "</a>";

        info.text = infoOut; // string.Format(info.text, recommended.ToString() + " " + stat, ((int)GameVariables.currentActivities[_index].difficulty).ToString(), GameVariables.currentActivities[_index].participatingTime.ToString() + "m");

    }

    public void OnTimeUpdate()
    {
        for (int i = 0; i < activityPosts.Length; i++)
        {
            activityPosts[i].UpdateDisplay(i);
        }
    }
}
