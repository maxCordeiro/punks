﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31;

public class PunkController : EntityController, ITimeUpdate
{
    //public Punk punk;
    CharacterController2D contr;

    Material mat;

    public Punk castEntity;// { get; private set; }

    void Awake()
    {
        contr = GetComponent<CharacterController2D>();

        mat = GetComponent<SpriteRenderer>().material;        
    }

    public new void Start()
    {
        base.Start();

        entityType = EntityType.PUNK;
    }

    public new void Update()
    {
        base.Update();

        castEntity = GetPunkEntity();
    }

    public void AssignPunk(Punk _punk)
    {
        canMove = false;

        entity = _punk;

        UpdatePunk(_punk);

        gameObject.name = "Punk - " + entity.entityName;

        canMove = true;
    }
    
    public void OnTimeUpdate()
    {
        if (GetPunkEntity().stamina < 100)
        {
            GetPunkEntity().stamina = Mathf.Clamp(GetPunkEntity().stamina + 5, 0, 100);
        }

        GetPunkEntity().Grow();
        UpdatePunk(GetPunkEntity());
    }

    public void UpdatePunk(Punk _punk)
    {
        GetComponent<SpriteRenderer>().sprite = PunkSystem.UpdatePunkSprite(_punk);
        GetComponent<SpriteRenderer>().material = PunkSystem.UpdatePunkMaterial(mat, _punk);
    }

    public override void OnClick()
    {
    }

    public override void OnClickUp()
    {
        GameController.instance.UpdatePunkWindow(GetPunkEntity());
    }

    public override void OnDragEntity()
    {
    }

    public override void OnItemLeftClick()
    {
        FeedPunk(true);
    }

    public override void OnItemRightClick()
    {
        FeedPunk(false);
    }

    void FeedPunk(bool fullStack)
    {
        Punk p = GetPunkEntity();

        if (p.age == PunkAge.SPROUT) return;

        int increaseAmount = (int)GameVariables.GetInventorySlot(GameController.instance.invController.GetSelectedCoords()).item.internalVars[1] * (fullStack? GameVariables.GetInventorySlot(GameController.instance.invController.GetSelectedCoords()).quantity : 1);

        bool canUse = p.IncreaseStat((PunkStat)GameVariables.GetInventorySlot(GameController.instance.invController.GetSelectedCoords()).item.internalVars[0], increaseAmount);

        if (canUse)
        {
            LeanTween.scale(gameObject, new Vector3(1, 1, 1), 0.2f).setFrom(new Vector3(1.2f, 0.8f, 1f));

            if (GameVariables.UseItem(GameController.instance.invController.GetSelectedCoords(), fullStack))
            {
                GameController.instance.invController.SelectInventoryItem(false);
            }
            else
            {
                GameController.instance.invController.SelectInventoryItem(true, GameController.instance.invController.inventorySlotArrayIndex);
            }
        }

        GameController.instance.UpdatePunkWindow(p);
        GameController.instance.invController.UpdateInventoryDisplay();
    }
}
