﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameVariables
{
    public static List<Punk> punks = new List<Punk>();
    public static ItemStack[,] inventory = new ItemStack[8,2];

    public static int gold;
    
    public static List<Activity> currentActivities = new List<Activity>();

    public static ActivityCannon cannonEntity;
    public static List<FuseMachine> fuseEntities = new List<FuseMachine>();
    public static List<Crop> cropEntities = new List<Crop>();

    public static bool AddItem(Item _item, int _quantity = 1)
    {
        return AddItem(new ItemStack(_item, _quantity));
    }

    public static bool AddItem(ItemStack _item)
    {
        for (int x = 0; x < inventory.GetLength(0); x++)
        {
            for (int y = 0; y < inventory.GetLength(1); y++)
            {
                if (inventory[x, y] == null)
                {
                    inventory[x, y] = _item;
                    return true;
                }
            }
        }

        return false;
    }
    
    public static bool UseItem(Coord invCoord, bool _wholeStack)
    {
        ItemStack _item = inventory[invCoord.x, invCoord.y];

        //Returns true if stack is consumed

        if (_item.item.maxStack == 1)
        {
            _item.currentDurability--;

            if (_item.currentDurability <= 0)
            {
                inventory[invCoord.x, invCoord.y] = null;
                return true;
            }
        }
        else
        {
            if (_wholeStack)
            {
                inventory[invCoord.x, invCoord.y] = null;
                return true;
            } else
            {
                _item.quantity--;

                if (_item.quantity <= 0)
                {
                    inventory[invCoord.x, invCoord.y] = null;
                    return true;
                }
            }
        }

        return false;
    }

    public static ItemStack GetInventorySlot(Coord _coord)
    {
        return inventory[_coord.x, _coord.y];
    }

    public static bool AddPunk(Punk _punk)
    {
        punks.Add(_punk);
        return true;
    }

    public static int GetHighestStat(PunkStat _stat)
    {
        int prevPeak = 0;

        for (int i = 0; i < punks.Count; i++)
        {
            if (punks[i].stats[(int)_stat] > prevPeak) prevPeak = punks[i].stats[(int)_stat];
        }

        return prevPeak;
    }

    public static bool CropExists(Vector2 pos)
    {
        foreach (Crop c in cropEntities)
        {
            if (c.pos == pos)
            {
                return true;
            }
        }

        return false;
    }
}

public enum GameState
{
    GARDEN,
    MENU_ACTIVITY,
    MENU_INVENTORY
}

[Serializable]
public struct Coord
{
    public int x, y;
}