﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ActivityPostUpdater : MonoBehaviour
{
    public SuperTextMesh activityTitle;
    public Image[] difficultyStars;
    public SuperTextMesh remainingTime;
    public Image punk;
    public Button button;

    public EventTrigger et;

    Material punkMaterial;

    private void Start()
    {
        punkMaterial = punk.material;
    }

    public void UpdateDisplay(int activityIndex)
    {
        activityTitle.text = GameDB.langActivityTitles[GameVariables.currentActivities[activityIndex].type];

        switch (GameVariables.currentActivities[activityIndex].difficulty)
        {
            case ActivityDifficulty.EASY:
                difficultyStars[0].gameObject.SetActive(true);
                difficultyStars[1].gameObject.SetActive(false);
                difficultyStars[2].gameObject.SetActive(false);
                difficultyStars[3].gameObject.SetActive(false);
                difficultyStars[4].gameObject.SetActive(false);
                break;
            case ActivityDifficulty.NORMAL:
                difficultyStars[0].gameObject.SetActive(true);
                difficultyStars[1].gameObject.SetActive(true);
                difficultyStars[2].gameObject.SetActive(false);
                difficultyStars[3].gameObject.SetActive(false);
                difficultyStars[4].gameObject.SetActive(false);
                break;
            case ActivityDifficulty.TOUGH:
                difficultyStars[0].gameObject.SetActive(true);
                difficultyStars[1].gameObject.SetActive(true);
                difficultyStars[2].gameObject.SetActive(true);
                difficultyStars[3].gameObject.SetActive(false);
                difficultyStars[4].gameObject.SetActive(false);
                break;
            case ActivityDifficulty.CHALLENGING:
                difficultyStars[0].gameObject.SetActive(true);
                difficultyStars[1].gameObject.SetActive(true);
                difficultyStars[2].gameObject.SetActive(true);
                difficultyStars[3].gameObject.SetActive(true);
                difficultyStars[4].gameObject.SetActive(false);
                break;
            case ActivityDifficulty.ELITE:
                difficultyStars[0].gameObject.SetActive(true);
                difficultyStars[1].gameObject.SetActive(true);
                difficultyStars[2].gameObject.SetActive(true);
                difficultyStars[3].gameObject.SetActive(true);
                difficultyStars[4].gameObject.SetActive(true);
                break;
            default:
                break;
        }


        if (GameVariables.currentActivities[activityIndex].isParticipating)
        {
            punk.gameObject.SetActive(true);
            punk.sprite = Resources.Load<Sprite>("PunkSprites/punk_body_" + GameVariables.currentActivities[activityIndex].playerPunk.shape.ToString().ToLower() + "_" + GameVariables.currentActivities[activityIndex].playerPunk.color.ToString().ToLower());

            Material newMat = new Material(punkMaterial);

            newMat.SetTexture("_Eye", Resources.Load<Texture>("PunkSprites/punk_eyes_" + GameVariables.currentActivities[activityIndex].playerPunk.eyes.ToString().ToLower() + "_" + GameVariables.currentActivities[activityIndex].playerPunk.shape.ToString().ToLower()));
            //newMat.SetColor("_PunkColor", GameDB.punkColors[(int)GameVariables.currentActivities[activityIndex].playerPunk.color]);

            punk.material = newMat;

            remainingTime.text = GameVariables.currentActivities[activityIndex].participatingTime.ToString() + "m";
        }
        else
        {
            punk.gameObject.SetActive(false);
            int h = GameVariables.currentActivities[activityIndex].remainingTime;
            remainingTime.text = (h == 60? "1h" : h.ToString() +  "m");
        }
        
    }
}
