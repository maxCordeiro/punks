﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public int slotIndex;
    public Coord slotCoord = new Coord();

    public SuperTextMesh stack, itemName;
    public Image sprite, durabilityBG, durabilityBar;
    public Button button;

    public InventoryController invController;

    // Start is called before the first frame update
    void Start()
    {
        button.onClick.AddListener(() => { ItemSelect(); });
    }

    public void ItemSelect()
    {
        if (invController.isSelectingItem)
        {
            if (slotIndex == invController.inventorySlotArrayIndex)
            {
                invController.SelectInventoryItem(false);
                invController.UpdateInventoryDisplay();
            } else if (GameVariables.inventory[slotCoord.x, slotCoord.y] == null)
            {
                ItemStack temp = GameVariables.GetInventorySlot(invController.GetSelectedCoords());

                GameVariables.inventory[invController.GetSelectedCoords().x, invController.GetSelectedCoords().y] = null;
                GameVariables.inventory[slotCoord.x, slotCoord.y] = temp;
                invController.SelectInventoryItem(false);
                invController.UpdateInventoryDisplay();
            }
        } else if (GameVariables.inventory[slotCoord.x, slotCoord.y] != null)
        {
            invController.SelectInventoryItem(true, slotIndex);
            sprite.color = new Color(1, 1, 1, 0.3f);
            invController.selectedItemSprite.sprite = sprite.sprite;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        itemName.gameObject.SetActive(GameVariables.inventory[slotCoord.x, slotCoord.y] != null);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        itemName.gameObject.SetActive(false);
    }
}