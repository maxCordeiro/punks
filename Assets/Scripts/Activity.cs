﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class Activity
{
    public List<ItemStack> rewards = new List<ItemStack>();

    public Punk playerPunk;
    public List<Punk> competitors = new List<Punk>();
    public bool isParticipating;

    public ActivityDifficulty difficulty;
    public ActivityType type;

    public int remainingTime; // in minutes, on activity board
    public int participatingTime; // time it takes for Punk to complete

    public Activity (ActivityType _type, ActivityDifficulty _difficulty)
    {
        type = _type;
        difficulty = _difficulty;
    }

    public Activity SetRemainingTime(int _time)
    {
        remainingTime = _time;
        return this;
    }

    public Activity AddCompetitors(params Punk[] _competitors)
    {
        competitors.AddRange(_competitors.ToList());
        return this;
    }

    public Activity AddRewards(params ItemStack[] _rewards)
    {
        rewards.AddRange(_rewards.ToList());
        return this;
    }
}

public enum ActivityType
{
    BOULDERPUSH,
    CHARM,
    SPELLINGBEE,
    RACE,
    SPELUNKING
}

public enum ActivityDifficulty
{
    EASY = 10,
    NORMAL = 20,
    TOUGH = 40,
    CHALLENGING = 65,
    ELITE = 85
}