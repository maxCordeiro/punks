﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class GameController : MonoBehaviour
{
    [Header("System")]
    public int timeMultiplier = 1;
    public Rewired.Player rePlayer;
    public GameObject cam;
    public GameState state;
    public GameState prevState;
    public Vector3 cursorPos;

    public InventoryController invController;

    Canvas gameCanvas;
    float timer;

    int fingerID = -1;
    
    [Header("Entity Controllers")]
    public List<PunkController> punkControllers = new List<PunkController>();
    public List<FuseMachineController> fuseControllers = new List<FuseMachineController>();
    public List<CropController> cropControllers = new List<CropController>();
    
    [Header("Entity Interaction")]
    public EntityController grabbedEntityController;
    public int indexOfPunk; // Index in GameVariables list of Punk selected
    public float grabbedEntityShadowY;
    public Vector3 grabbedEntityStartPos;
    public EntityType grabbedEntityType;

    // Stat Window
    [Header("Stat Window")]
    public GameObject statWindow;
    public SuperTextMesh punkName;
    public SuperTextMesh[] statLetters;
    public SuperTextMesh[] statNames;
    public SuperTextMesh[] statAmounts;

    [Header("Misc UI")]
    public SuperTextMesh entityHeadsUp;
    RectTransform entityHeadsUpRect;

    public Image gridOutlineSprite;

    public List<UISystem> openMenus = new List<UISystem>();

    private static GameController _instance;
    public static GameController instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

#if !UNITY_EDITOR
        fingerID = 0; 
#endif
    }

    // Start is called before the first frame update
    void Start()
    {
        state = GameState.GARDEN;

        cam = Camera.main.gameObject;

        rePlayer = Rewired.ReInput.players.GetPlayer(0);

        entityHeadsUpRect = entityHeadsUp.GetComponent<RectTransform>();

        gameCanvas = GameObject.FindGameObjectWithTag("GameCanvas").GetComponent<Canvas>();

        GameDB.BuildItemDB();

        GameVariables.AddItem(GameDB.itemDB["muscleberry"], 10);
        GameVariables.AddItem(GameDB.itemDB["hoe"], 1);
        GameVariables.AddItem(GameDB.itemDB["seed_muscleberry"], 1);

        Punk p0 = new Punk("Cork")
        {
            age = PunkAge.ACTIVE
        };

        Punk p1 = new Punk("Beef")
        {
            age = PunkAge.ACTIVE
        };

        Punk p2 = new Punk("Bell")
        {
            age = PunkAge.ACTIVE
        };

        GameVariables.AddPunk(p0);
        GameVariables.AddPunk(p1);
        GameVariables.AddPunk(p2);
        GameVariables.AddPunk(new Punk());
        GameVariables.AddPunk(new Punk());
        GameVariables.AddPunk(new Punk());
        GameVariables.AddPunk(new Punk());

        Crop c = new Crop()
        {
            growthTime = 0,
            doneGrowing = true,
            pos = new Vector2(-3, 1),
        };

        c.AddPunk(new Punk("Jabroni"), 1);

        GameVariables.cropEntities.Add(c);

        FuseMachine fm = new FuseMachine
        {
            pos = new Vector2(Random.Range(-2f, 2f), Random.Range(-2f, 2f))
        };

        GameVariables.fuseEntities.Add(fm);
        GameVariables.cannonEntity = new ActivityCannon();

        GenerateNewActivity();
        GenerateNewActivity();
        GenerateNewActivity();
        GenerateNewActivity();
        GenerateNewActivity();
        GenerateNewActivity();

        UpdatePunkControllers();
        UpdateFuseMachineControllers();
        UpdateCropControllers();
        UpdateActivityCannonController();

    }

    // Update is called once per frame
    void Update()
    {           
        switch (state)
        {
            case GameState.GARDEN:
                CursorControl();
                TimeControl();
                DebugKeys();
                CameraControl();
                MenuControl();
                break;
            case GameState.MENU_ACTIVITY:
                TimeControl();
                break;
            default:
                break;
        }
        
    }

    public void MenuControl()
    {
        if (rePlayer.GetButtonDown("Action"))
        {
            invController.OpenInventory();
        }
    }
    
    void CameraControl()
    {
        cam.transform.position += Vector3.ClampMagnitude(new Vector3(rePlayer.GetAxis("Horizontal"), rePlayer.GetAxis("Vertical")), 1) * Time.deltaTime * 10;
        
        cam.transform.position = Util.ClampVector(cam.transform.position, -3, 2.88f, 3, -3.37f, -10);
        
    }

    void TimeControl()
    {
        timer += Time.deltaTime * timeMultiplier;

        if (timer > 60)
        {
            MinutePassed();
            timer -= 60;
        }

    }

    void DebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            GameVariables.AddPunk(new Punk(Random.Range(0, 666666).ToString()));
            UpdatePunkControllers();
        }

        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            Time.timeScale -= 0.1f;
        } else if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            Time.timeScale += 0.1f;
        }
    }

    public void CursorControl()
    {
        RaycastHit2D _hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);//, 10, mask);

        cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        cursorPos.z = 0;

        if (invController.isSelectingItem)
        {
            UseItem(_hit);
        } else
        {
            InteractWithEntity(_hit);
        }

        
    }

    void UseItem(RaycastHit2D _hit)
    {
        Debug.Log("UseItem");

        invController.selectedItemSprite.rectTransform.position = cursorPos + (invController.selectedItemType == ItemType.TOOL ? new Vector3(0.5f, -0.5f) : Vector3.zero);

        if (invController.selectedItemType == ItemType.TOOL)
        {
            gridOutlineSprite.enabled = true;
            gridOutlineSprite.rectTransform.position = Util.RoundToGrid(cursorPos);
        }
        else
        {
            gridOutlineSprite.enabled = false;
        }

        if (_hit && !EventSystem.current.IsPointerOverGameObject(fingerID))
        {
            var hitCont = _hit.transform.GetComponent<EntityController>();

            if (hitCont != null)
            {
                if (rePlayer.GetButtonDown("PrimaryClick"))
                {
                    hitCont.OnItemLeftClick();
                }
                else if (rePlayer.GetButtonDown("SecondaryClick"))
                {
                    hitCont.OnItemRightClick();
                }
            }
            else
            {
                if (rePlayer.GetButtonDown("PrimaryClick"))
                {
                    OnItemLeftClick(_hit);
                }
                else if (rePlayer.GetButtonDown("SecondaryClick"))
                {
                    OnItemRightClick(_hit);
                }
            }
        }

    }

    void InteractWithEntity(RaycastHit2D _hit)
    {
        if (gridOutlineSprite.enabled)  gridOutlineSprite.enabled = false;

        if (grabbedEntityController)
        {

            if (rePlayer.GetButton("CtrlMod"))
            {
                cursorPos = Util.RoundToGrid(cursorPos);
            }

            MoveGrabbedEntity(cursorPos);

            UpdateHeadsUp(true, grabbedEntityController.transform);
        }
        else
        {
            UpdateHeadsUp(_hit, _hit.transform);
        }

        if (rePlayer.GetButtonTimedPress("PrimaryClick", 0.2f) && !EventSystem.current.IsPointerOverGameObject(fingerID) && !invController.isSelectingItem)
        {
            if (_hit && !grabbedEntityController)
            {
                var controller = _hit.transform.GetComponent<EntityController>();

                if (controller != null && controller.canDrag)
                {
                    grabbedEntityController = controller;
                    grabbedEntityType = controller.entityType;

                    if (controller.GetType() == typeof(PunkController))
                    {
                        indexOfPunk = GameVariables.punks.IndexOf(grabbedEntityController.GetPunkEntity());
                    }

                    grabbedEntityStartPos = grabbedEntityController.entity.pos;
                    grabbedEntityShadowY = _hit.transform.GetChild(0).localPosition.y;
                }

            }
        }
        else if (rePlayer.GetButtonUp("PrimaryClick") && !EventSystem.current.IsPointerOverGameObject(fingerID))
        {
            //Vector3 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (grabbedEntityController)
            {
                if (_hit)
                {
                    var hitCont = _hit.transform.GetComponent<EntityController>();

                    if (hitCont != null)
                    {
                        hitCont.OnDragEntity();
                    }
                    else
                    {
                        ReleaseGrabbedEntity();
                    }

                }
            }
            else
            {
                if (_hit)
                {
                    var controller = _hit.transform.GetComponent<EntityController>();

                    if (controller != null)
                    {
                        controller.OnClickUp();
                    }
                    else if (_hit.transform.tag == "BG")
                    {
                        ClosePunkWindow();
                    }
                }
            }
        }
    }

    void UpdateHeadsUp(bool enabled, Transform pos)
    {
        if (enabled && pos.tag != "BG")
        {
            if (!entityHeadsUp.gameObject.activeInHierarchy) entityHeadsUp.gameObject.SetActive(true);
            
            entityHeadsUpRect.position = new Vector3(pos.position.x, pos.position.y + 2);
            entityHeadsUpRect.localPosition = new Vector3(Mathf.RoundToInt(entityHeadsUpRect.localPosition.x), Mathf.RoundToInt(entityHeadsUpRect.localPosition.y));

            entityHeadsUp.text = pos.GetComponent<EntityController>().entity.entityName;
            
        } else
        {
            entityHeadsUp.gameObject.SetActive(false);
        }
    }

    public void UpdatePunkWindow(Punk p)
    {
        punkName.text = p.entityName;

        for (int i = 0; i < p.stats.Length; i++)
        {
            statLetters[i].text = p.statGrade[i].ToString();
            statAmounts[i].text = p.stats[i].ToString();
        }

        statAmounts[statAmounts.Length - 1].text = p.stamina.ToString();

        if (!statWindow.activeInHierarchy)
        {
            statWindow.SetActive(true);
            LeanTween.moveLocalX(statWindow, 112, 0.1f).setFrom(240);
        }
    }

    public void ClosePunkWindow()
    {
        LeanTween.moveLocalX(statWindow, 240, 0.1f).setOnComplete(() => { statWindow.SetActive(false); });
    }
    
    public void MinutePassed()
    {
        UpdateCrops();
        UpdatePunks();
        UpdateActivities();
        UpdateMenus();
    }

    void UpdateMenus()
    {
        foreach (UISystem ui in openMenus)
        {
            ui.SendMessage("OnTimeUpdate");
        }
    }

    void UpdateCrops()
    {
        for (int i = 0; i < GameVariables.cropEntities.Count; i++)
        {
            if (GameVariables.cropEntities[i].cropType == CropType.NONE || GameVariables.cropEntities[i].doneGrowing) continue;

            GameVariables.cropEntities[i].growthTime -= 1;

            if (GameVariables.cropEntities[i].growthTime <= 0)
            {
                GameVariables.cropEntities[i].doneGrowing = true;
            }

        }

        UpdateCropControllers();
    }

    void UpdatePunks()
    {
        foreach (PunkController pc in punkControllers)
        {
            pc.OnTimeUpdate();
        }

        UpdatePunkControllers();
    }

    void UpdateActivities()
    {
        for (int i = 0; i < GameVariables.currentActivities.Count; i++)
        {
            if (GameVariables.currentActivities[i].isParticipating)
            {
                GameVariables.currentActivities[i].participatingTime -= 1;

                if (GameVariables.currentActivities[i].participatingTime <= 0)
                {
                    GameVariables.currentActivities[i].playerPunk.UseStamina((int)GameVariables.currentActivities[i].difficulty);

                    if (Random.Range(0f, 1f) >= 0.2f)
                    {
                        // WIN

                        for (int r = 0; r < GameVariables.currentActivities[i].rewards.Count; r++)
                        {
                            GameVariables.AddItem(GameVariables.currentActivities[r].rewards[r]);
                        }

                        GameVariables.AddPunk(GameVariables.currentActivities[i].playerPunk);
                    }

                    GameVariables.currentActivities.RemoveAt(i);
                    UpdatePunkControllers();

                    GenerateNewActivity();
                }
            } else
            {
                GameVariables.currentActivities[i].remainingTime -= 1;

                if (GameVariables.currentActivities[i].remainingTime <= 0)
                {
                    // Generate New Activity
                    GameVariables.currentActivities.RemoveAt(i);
                    GenerateNewActivity();
                }
            }
        }
    }

    public void GenerateNewActivity()
    {
        PunkStat randomStat = (PunkStat)Random.Range(0, 5);
        int highestStat = GameVariables.GetHighestStat(randomStat);

        ActivityDifficulty difficulty = ActivityDifficulty.EASY;

        if (highestStat < 100)
        {
            difficulty = ActivityDifficulty.EASY;
        } else if (highestStat >= 100 && highestStat < 300)
        {
            difficulty = ActivityDifficulty.NORMAL;
        } else if (highestStat >= 300 && highestStat < 500)
        {
            difficulty = ActivityDifficulty.TOUGH;
        } else if (highestStat >= 500 && highestStat < 750)
        {
            difficulty = ActivityDifficulty.CHALLENGING;
        } else if (highestStat >= 750)
        {
            difficulty = ActivityDifficulty.ELITE;
        }
        
        Activity newActivity = new Activity((ActivityType)randomStat, difficulty);

        List<ItemStack> tempRewards = new List<ItemStack>();

        /*int rewardsAmount = 0;

        switch (difficulty)
        {
            case ActivityDifficulty.EASY:
                rewardsAmount = Random.Range(1, 4);
                break;
            case ActivityDifficulty.NORMAL:
                rewardsAmount = Random.Range(2, 6);
                break;
            case ActivityDifficulty.TOUGH:
                rewardsAmount = Random.Range(6, 9);
                break;
            case ActivityDifficulty.CHALLENGING:
                rewardsAmount = Random.Range(9, 11);
                break;
            case ActivityDifficulty.ELITE:
                rewardsAmount = Random.Range(11, 14);
                break;
            default:
                break;
        }*/

        /*for (int i = 0; i < rewardsAmount; i++)
        {
            switch (randomStat)
            {
                case PunkStat.MUSCLE:
                    break;
                case PunkStat.CHARM:
                    break;
                case PunkStat.SMART:
                    break;
                case PunkStat.DEX:
                    break;
                case PunkStat.LUCK:
                    break;
                default:
                    break;
            }
        }*/

        switch (randomStat)
        {
            case PunkStat.MUSCLE:
                tempRewards.Add(new ItemStack("muscleberry", 5));
                break;
            case PunkStat.CHARM:
                tempRewards.Add(new ItemStack("charmberry", 5));
                break;
            case PunkStat.SMART:
                tempRewards.Add(new ItemStack("smartberry", 5));
                break;
            case PunkStat.DEX:
                tempRewards.Add(new ItemStack("dexberry", 5));
                break;
            case PunkStat.LUCK:
                tempRewards.Add(new ItemStack("luckberry", 5));
                break;
            default:
                break;
        }

        newActivity.rewards = tempRewards;

        int waitTime = -1;

        switch (difficulty)
        {
            case ActivityDifficulty.EASY:
                waitTime = 10;
                break;
            case ActivityDifficulty.NORMAL:
                waitTime = 20;
                break;
            case ActivityDifficulty.TOUGH:
                waitTime = 40;
                break;
            case ActivityDifficulty.CHALLENGING:
                waitTime = 55;
                break;
            case ActivityDifficulty.ELITE:
                waitTime = 70;
                break;
            default:
                break;
        }

        newActivity.participatingTime = waitTime;
        newActivity.remainingTime = 60;

        GameVariables.currentActivities.Add(newActivity);
    }

    public void UpdatePunkControllers()
    {
        grabbedEntityController = null;
        indexOfPunk = -1;

        int offset;

        if (punkControllers.Count > GameVariables.punks.Count)
        {
            offset = (punkControllers.Count - GameVariables.punks.Count) ;

            for (int i = 0; i < offset; i++)
            {
                Destroy(punkControllers[i].gameObject);
                punkControllers.RemoveAt(i);
            }

        } else if (punkControllers.Count < GameVariables.punks.Count)
        {
            offset = GameVariables.punks.Count - punkControllers.Count;
            for (int i = 0; i < offset; i++)
            {
                // TEMPORARY RANDOM POS
                PunkController p = Instantiate(Resources.Load<GameObject>("Punk"), new Vector3(Random.Range(-10f, 10f), Random.Range(-5f, 5f)), Quaternion.identity).GetComponent<PunkController>();
                p.AssignPunk(GameVariables.punks[i]);
                
                punkControllers.Add(p);
            }
        }

        punkControllers.RemoveAll(item => item == null);
        
        for (int i = 0; i < punkControllers.Count; i++)
        {
            if (punkControllers[i].GetPunkEntity() != GameVariables.punks[i])
            {
                punkControllers[i].AssignPunk(GameVariables.punks[i]);
                punkControllers[i].UpdateEntityPos();
            }

            punkControllers[i].transform.GetChild(0).transform.localPosition = new Vector3(0, -0.18f, 0);
            punkControllers[i].GetComponent<SpriteRenderer>().sortingOrder = 0;
            punkControllers[i].transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 0;
            punkControllers[i].transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Default";
            
        }
        
    }

    public void UpdateCropControllers()
    {
        grabbedEntityController = null;
        indexOfPunk = -1;

        int offset;

        if (cropControllers.Count > GameVariables.cropEntities.Count)
        {
            offset = (cropControllers.Count - GameVariables.cropEntities.Count);

            for (int i = 0; i < offset; i++)
            {
                Destroy(cropControllers[i].gameObject);
                cropControllers.RemoveAt(i);
            }
        } else if (cropControllers.Count < GameVariables.cropEntities.Count)
        {
            offset = GameVariables.cropEntities.Count - cropControllers.Count;

            for (int i = 0; i < offset; i++)
            {
                CropController c = Instantiate(Resources.Load<GameObject>("CropController")).GetComponent<CropController>();
                c.AssignEntity(GameVariables.cropEntities[i]);

                cropControllers.Add(c);
            }
        }

        cropControllers.RemoveAll(item => item == null);

        for (int i = 0; i < cropControllers.Count; i++)
        {
            if (cropControllers[i].GetCropEntity() != GameVariables.cropEntities[i])
            {
                cropControllers[i].AssignEntity(GameVariables.cropEntities[i]);
                cropControllers[i].UpdateEntityPos();
            }
        }
    }

    public void UpdateFuseMachineControllers()
    {
        grabbedEntityController = null;
        indexOfPunk = -1;

        int offset;

        if (fuseControllers.Count > GameVariables.fuseEntities.Count)
        {
            offset = (fuseControllers.Count - GameVariables.fuseEntities.Count);

            for (int i = 0; i < offset; i++)
            {
                Destroy(fuseControllers[i].gameObject);
                fuseControllers.RemoveAt(i);
            }
        } else if (fuseControllers.Count < GameVariables.fuseEntities.Count)
        {
            offset = GameVariables.fuseEntities.Count - fuseControllers.Count;

            for (int i = 0; i < offset; i++)
            {
                FuseMachineController f = Instantiate(Resources.Load<GameObject>("FuseMachine")).GetComponent<FuseMachineController>();
                f.AssignEntity(GameVariables.fuseEntities[i]);

                fuseControllers.Add(f);
            }
        }

        fuseControllers.RemoveAll(item => item == null);

        for (int i = 0; i < fuseControllers.Count; i++)
        {
            if (fuseControllers[i].GetFuseMachineEntity() != GameVariables.fuseEntities[i])
            {
                fuseControllers[i].AssignEntity(GameVariables.fuseEntities[i]);
                fuseControllers[i].UpdateEntityPos();
            }
        }
    }

    public void UpdateActivityCannonController()
    {
        ActivityCannonController a = Instantiate(Resources.Load<GameObject>("ActivityCannon")).GetComponent<ActivityCannonController>();
        a.entity = GameVariables.cannonEntity;
    }

    public void MoveGrabbedEntity(Vector2 pos)
    {
        grabbedEntityController.GetComponent<SpriteRenderer>().sortingOrder = 10;
        grabbedEntityController.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 10;
        grabbedEntityController.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Objects";
        grabbedEntityController.transform.position = cursorPos + new Vector3(0, 1);
        grabbedEntityController.transform.GetChild(0).position = grabbedEntityController.transform.position - new Vector3(0, 1);

    }

    public void ReleaseGrabbedEntity(bool returnToStart = false)
    {
        grabbedEntityController.GetComponent<SpriteRenderer>().sortingOrder = 0;
        grabbedEntityController.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 0;
        grabbedEntityController.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Default";
        grabbedEntityController.transform.GetChild(0).localPosition = new Vector3(0, grabbedEntityShadowY, 0);

        LeanTween.move(grabbedEntityController.gameObject, returnToStart ? grabbedEntityStartPos : cursorPos, 0.12f);
        
        grabbedEntityController = null;
        grabbedEntityType = EntityType.NONE;
        grabbedEntityShadowY = 0;
    }
    
    public void ChangeState(GameState newState)
    {
        prevState = state;
        state = newState;
    }

    public void OnItemLeftClick(RaycastHit2D hit)
    {
        if (hit.transform.tag == "BG" && !GameVariables.CropExists(Util.RoundToGrid(cursorPos)))
        {
            Crop c = new Crop
            {
                pos = Util.RoundToGrid(cursorPos)
            };

            GameVariables.cropEntities.Add(c);
            if (GameVariables.UseItem(invController.inventorySlots[invController.inventorySlotArrayIndex].slotCoord, false))
            {
                invController.SelectInventoryItem(false);
            }
            else
            {
                invController.SelectInventoryItem(true, invController.inventorySlotArrayIndex);
            }

            invController.UpdateInventoryDisplay();

            UpdateCropControllers();
        }
    }

    public void OnItemRightClick(RaycastHit2D hit)
    {

    }
}

public enum EntityType
{
    NONE,
    PUNK,
    ACTIVITY,
    FUSE,
    CROP
}