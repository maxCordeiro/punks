﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivityCannonController : EntityController
{
    public new void Start()
    {
        base.Start();

        entityType = EntityType.ACTIVITY;
    }

    public override void OnClick()
    {

    }

    public override void OnClickUp()
    {
        GameController.instance.ClosePunkWindow();

        ActivityMenu act = Instantiate<GameObject>(Resources.Load<GameObject>("ActivityMenu"), GameObject.FindGameObjectWithTag("GameCanvas").transform).GetComponent<ActivityMenu>();
        act.gameObject.SetActive(true);

        GameController.instance.openMenus.Add(act);

        GameController.instance.ChangeState(GameState.MENU_ACTIVITY);
    }

    public override void OnDragEntity()
    {
        if (GameController.instance.grabbedEntityType != EntityType.PUNK)
        {
            GameController.instance.ReleaseGrabbedEntity(true);
            return;
        }

        if (GameController.instance.grabbedEntityController.GetPunkEntity().age != PunkAge.SPROUT)
        {
            ActivityMenu act = Instantiate<GameObject>(Resources.Load<GameObject>("ActivityMenu"), GameObject.FindGameObjectWithTag("GameCanvas").transform).GetComponent<ActivityMenu>();
            act.gameObject.SetActive(true);

            GameController.instance.openMenus.Add(act);
           
            //GameController.instance.ReleaseGrabbedEntity();

            GameController.instance.ChangeState(GameState.MENU_ACTIVITY);
        } else
        {
            GameController.instance.ReleaseGrabbedEntity(true);
            return;
        }
    }

    public override void OnItemLeftClick()
    {

    }

    public override void OnItemRightClick()
    {

    }
}
