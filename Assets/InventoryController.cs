﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.U2D.Path.GUIFramework;
using UnityEngine;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour, ITimeUpdate
{
    [Header("References")]
    public GameObject inventoryWindow;
    public InventorySlot[] inventorySlots;
    public Image selectedItemSprite, selectedItemDurBG, selectedItemDurBar;
    public SuperTextMesh selectedItemQuantity;

    [Header("Logic")]
    public bool isInventoryOpen;
    public bool isSelectingItem;
    public int inventorySlotArrayIndex; // Array index of inventory slot that is clicked
    public ItemType selectedItemType;


    // Start is called before the first frame update
    void Start()
    {
        inventorySlotArrayIndex = -1;

        for (int i = 0; i < 8; i++)
        {
            inventorySlots[i].slotCoord.x = i;
            inventorySlots[i + 8].slotCoord.x = i;

            inventorySlots[i].slotCoord.y = 0;
            inventorySlots[i + 8].slotCoord.y = 1;
        }

        for (int i = 0; i < 16; i++)
        {
            inventorySlots[i].slotIndex = i;
            inventorySlots[i].invController = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void OpenInventory()
    {
        if (isInventoryOpen)
        {
            isSelectingItem = false;
            isInventoryOpen = false;
            inventoryWindow.gameObject.SetActive(false);
            SelectInventoryItem(false);
        }
        else
        {
            inventoryWindow.gameObject.SetActive(true);
            UpdateInventoryDisplay();
            isInventoryOpen = true;
        }
    }

    public void SelectInventoryItem(bool _isSelectingItem, int _inventorySlotArrayIndex = -1)
    {
        isSelectingItem = _isSelectingItem;

        ItemStack tempStack = null;

        if (_isSelectingItem)
        {
            tempStack = GameVariables.GetInventorySlot(inventorySlots[_inventorySlotArrayIndex].slotCoord);

            selectedItemType = tempStack.item.type;

            float maxDur = tempStack.item.maxDurability;
            float curDur = tempStack.currentDurability;

            if (tempStack.item.maxStack == 1 && maxDur != curDur)
            {
                selectedItemDurBG.enabled = true;
                selectedItemDurBar.enabled = true;
                selectedItemDurBar.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Floor(12 * (float)(curDur/ maxDur)));
            } else
            {
                selectedItemDurBG.enabled = false;
                selectedItemDurBar.enabled = false;
            }
        } else
        {
            selectedItemDurBG.enabled = false;
            selectedItemDurBar.enabled = false;
        }

        inventorySlotArrayIndex = isSelectingItem ? _inventorySlotArrayIndex : -1;
        selectedItemSprite.color = isSelectingItem ? Color.white : Color.clear;
        selectedItemQuantity.text = isSelectingItem ? tempStack.quantity > 0 && tempStack.item.maxStack > 1 ? tempStack.quantity.ToString() : "" : "";

    }

    public void UpdateInventoryDisplay()
    {
        for (int i = 0; i < inventorySlots.Length; i++)
        {
            ItemStack tempStack = GameVariables.GetInventorySlot(inventorySlots[i].slotCoord);

            if (tempStack != null)
            {
                inventorySlots[i].sprite.sprite = Resources.Load<Sprite>("Items/" + tempStack.item.id);
                inventorySlots[i].sprite.color = inventorySlotArrayIndex == i ? new Color(1, 1, 1, 0.3f) : Color.white;
                inventorySlots[i].sprite.enabled = true;
                inventorySlots[i].stack.text = tempStack.quantity > 0 && tempStack.item.maxStack > 1 ? tempStack.quantity.ToString() : "";
                inventorySlots[i].itemName.text = GameDB.langItems[tempStack.item.id];

                float maxDurability = tempStack.item.maxDurability;
                float currDurability = tempStack.currentDurability;

                if (tempStack.item.maxStack == 1)
                {
                    inventorySlots[i].durabilityBG.enabled = maxDurability == currDurability;
                    inventorySlots[i].durabilityBar.enabled = maxDurability == currDurability;

                    inventorySlots[i].durabilityBar.rectTransform.sizeDelta = new Vector2(Mathf.Floor(12 * (float)(currDurability / maxDurability)), 2);
                }
                else
                {
                    inventorySlots[i].durabilityBG.enabled = false;
                    inventorySlots[i].durabilityBar.enabled = false;
                }
            }
            else
            {
                inventorySlots[i].sprite.sprite = null;
                inventorySlots[i].sprite.color = Color.clear;
                inventorySlots[i].stack.text = "";

                inventorySlots[i].durabilityBG.enabled = false;
                inventorySlots[i].durabilityBar.enabled = false;
            }
        }
    }

    public void OnTimeUpdate()
    {
        // just in case idk
    }

    public Coord GetSelectedCoords()
    {
        return inventorySlots[inventorySlotArrayIndex].slotCoord;
    }
}
