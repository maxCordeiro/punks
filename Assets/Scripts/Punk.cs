﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[System.Serializable]
public class Punk : Entity
{
    // Stats
    //public int muscle, charm, smart, dex, luck, stamina;
    public int[] stats = new int[5];
    public int stamina;
    public StatGrade[] statGrade = new StatGrade[5];
    
    public PunkColor color;
    public PunkShape shape;
    public PunkEyes eyes;

    public float happiness;

    public int activeMinutes;
    public PunkAge age;
        
    public Punk(string _nickname = "")
    {
        entityName = _nickname == ""? "Punk #" + Random.Range(0, 1000) : _nickname;

        color = (PunkColor)Random.Range(0, 3);

        shape = (PunkShape)Random.Range(0, Enum.GetNames(typeof(PunkShape)).Length);
        eyes = (PunkEyes)Random.Range(0, Enum.GetNames(typeof(PunkEyes)).Length);

        pos = new Vector3(Random.Range(-10f, 10f), Random.Range(-5f, 5f));

        activeMinutes = 0;
        age = PunkAge.SPROUT;

        CalcStats();
    }

    public Punk CalcStats()
    {
        for (int i = 0; i < stats.Length; i++)
        {
            stats[i] = Random.Range(0, 10);
            statGrade[i] = (StatGrade)Random.Range(1, 7);
        }

        stamina = 50;
        
        return this;
    }

    public Punk UseStamina(int _amt)
    {
        stamina = Mathf.Clamp(stamina - _amt, 0, 100);

        return this;
    }

    public bool IncreaseStat(PunkStat _index, int _amt)
    {
        if (stats[(int)_index] == 999)
        {
            return false;
        } else
        {
            stats[(int)_index] = Mathf.Clamp(stats[(int)_index] + _amt, 0, 999);
            return true;
        }
    }

    public PunkAge Grow(int _amt = 1)
    {
        activeMinutes += _amt;

        if (activeMinutes > (int)age)
        {
            switch (age)
            {
                case PunkAge.SPROUT:
                    age = PunkAge.ACTIVE;
                    break;
                case PunkAge.ACTIVE:
                    age = PunkAge.ELDER;
                    break;
                case PunkAge.ELDER:
                    age = PunkAge.DEAD;
                    break;
                default:
                    break;
            }
        }

        return age;
    }
}

public static class PunkSystem
{
    public static Punk CalcStats(Punk _p)
    {
        Punk tempPunk = _p;

        for (int i = 0; i < tempPunk.stats.Length; i++)
        {
            tempPunk.stats[i] = Random.Range(0, 10);
            tempPunk.statGrade[i] = (StatGrade)Random.Range(1, 7);
        }

        return _p;
    }

    public static Punk FusePunks(Punk _a, Punk _b, ItemStack _item)
    {
        Punk c = new Punk();

        //c.color = CombineColors(_a.color, _b.color);
        
        for (int i = 0; i < _a.statGrade.Length; i++)
        {
            c.statGrade[i] = CombineGrades(_a.statGrade[i], _b.statGrade[i]);
        }

        return c;
    }

    public static StatGrade CombineGrades(StatGrade _a, StatGrade _b)
    {
        return (StatGrade)Mathf.FloorToInt(((int)_a + (int)_b)/2);
    }

    public static PunkColor CombineColors(PunkColor _a, PunkColor _b)
    {
        PunkColor c = PunkColor.BLACK;

        if (_a == PunkColor.RED)
        {
            switch (_b)
            {
                case PunkColor.RED:
                    return PunkColor.RED;
                case PunkColor.YELLOW:
                    return PunkColor.ORANGE;
                case PunkColor.BLUE:
                    return PunkColor.PURPLE;
                case PunkColor.ORANGE:
                    return Util.Choose(PunkColor.RED, PunkColor.ORANGE);
                case PunkColor.GREEN:
                    return PunkColor.BROWN;
                case PunkColor.PINK:
                    return Util.Choose(PunkColor.RED, PunkColor.PINK);
                case PunkColor.PURPLE:
                    return Util.Choose(PunkColor.RED, PunkColor.PURPLE);
                case PunkColor.GOLD:
                    return PunkColor.GOLD;
                case PunkColor.WHITE:
                    return PunkColor.PINK;
                case PunkColor.BLACK:
                    return PunkColor.RED;
            }
        }

        return c;
    }

    public static void SendToActivity(int _playerPunk, int _activityIndex)
    {
        GameVariables.currentActivities[_activityIndex].playerPunk = GameVariables.punks[_playerPunk];
        GameVariables.currentActivities[_activityIndex].isParticipating = true;

        GameVariables.punks.RemoveAt(_playerPunk);
    }

    public static Material UpdatePunkMaterial(Material _mat, Punk _punk)
    {
        Material mat = new Material(_mat);
        /*mat.SetTexture("_Eye", Resources.Load<Texture>("PunkSprites/punk_" + 
            (_punk.age == PunkAge.SPROUT? 
            "sprout_" + _punk.shape.ToString().ToLower() + "_" + _punk.color.ToString().ToLower() 
            : 
            "eyes_" + _punk.eyes.ToString().ToLower() + "_" + _punk.shape.ToString().ToLower()
            )));*/

        if (_punk.age == PunkAge.SPROUT)
        {
            mat.SetTexture("_Eye", Resources.Load<Texture>("PunkSprites/punk_sprout_" + _punk.shape.ToString().ToLower() + "_" + _punk.color.ToString().ToLower()));
        } else
        {
            mat.SetTexture("_Eye", Resources.Load<Texture>("PunkSprites/punk_eyes_" + _punk.eyes.ToString().ToLower() + "_" + _punk.shape.ToString().ToLower()));
        }

        return mat;
    }

    public static Sprite UpdatePunkSprite(Punk _punk)
    {
        Sprite punkSprite;
        punkSprite = Resources.Load<Sprite>("PunkSprites/punk_" + (_punk.age == PunkAge.SPROUT? "sprout_" : "body_") + _punk.shape.ToString().ToLower() + "_" + _punk.color.ToString().ToLower());
        
        return punkSprite;
    }
}

public enum StatGrade
{
    S = 6,
    A = 5,
    B = 4,
    C = 3,
    D = 2,
    F = 1
}

public enum PunkShape
{
    CIRCLE,
    SQUARE,
    TRIANGLE
}

public enum PunkEyes
{
    DOT,
    SLANT,
    BIG,
    TIRED
}

public enum PunkStat
{
    MUSCLE,
    CHARM,
    SMART,
    DEX,
    LUCK
}

public enum PunkColor
{
    RED,
    YELLOW,
    BLUE,
    ORANGE,
    GREEN,
    PINK,
    PURPLE,
    BROWN,
    GOLD,
    WHITE,
    BLACK
}

public enum PunkAge
{
    SPROUT = 15,
    ACTIVE = 120,
    ELDER = 160,
    DEAD = 161
}