﻿using Rewired.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class CropController : EntityController, ITimeUpdate
{
    public Sprite[] growthSprites;
    bool growAnim;

    public new void Start()
    {
        base.Start();

        entityType = EntityType.CROP;
        canDrag = false;
    }

    public new void Update()
    {
        base.Update();

        if (GetCropEntity().doneGrowing && !growAnim)
        {
            growAnim = true;
            var seq = LeanTween.sequence();
            seq.append(LeanTween.scale(gameObject, new Vector3(1.1f, 0.9f, 1), 0.15f));
            seq.append(LeanTween.scale(gameObject, Vector3.one, 0.15f));          
            seq.append(LeanTween.scale(gameObject, new Vector3(0.9f, 1.1f, 1), 0.15f));
            seq.append(LeanTween.scale(gameObject, Vector3.one, 0.15f));
            seq.append(() => { growAnim = false; });
        }
    }

    public void OnTimeUpdate()
    {
        if (entity == null) return;

        ((Crop)entity).growthTime -= 1;

        if (((Crop)entity).growthTime <= 0)
        {
            // Hatch
        }
    }

    public void UpdateSprite(int growthPhase)
    {
        SpriteRenderer s = GetComponent<SpriteRenderer>();

        s.sprite = growthSprites[growthPhase];
    }

    public override void OnClick()
    {
    }

    public override void OnClickUp()
    {
        if (GetCropEntity().doneGrowing)
        {
            if (GetCropEntity().cropType == CropType.ITEM)
            {
                GameVariables.AddItem(GetCropEntity().cropItem);
                GameController.instance.invController.UpdateInventoryDisplay();

            }
            else if (GetCropEntity().cropType == CropType.PUNK)
            {
                GameVariables.AddPunk(GetCropEntity().cropPunk);
                GameController.instance.UpdatePunkControllers();
            }

            GameVariables.cropEntities.RemoveAt(GameVariables.cropEntities.IndexOf(GetCropEntity()));

            GameController.instance.UpdateCropControllers();
        }
    }

    public override void OnDragEntity() 
    {

    }

    public override void OnItemLeftClick()
    {
        if (GameVariables.GetInventorySlot(GameController.instance.invController.GetSelectedCoords()).item.type != ItemType.SEED) return;

        Coord tempCoord = GameController.instance.invController.inventorySlots[GameController.instance.invController.inventorySlotArrayIndex].slotCoord;

        GetCropEntity().AddItem(new ItemStack(GameDB.itemDB[(string)GameVariables.inventory[tempCoord.x, tempCoord.y].item.internalVars[0]], 1), 1);

        UpdateSprite(1);

        if (GameVariables.UseItem(tempCoord, false))
        {
            GameController.instance.invController.SelectInventoryItem(false);
        }
        else
        {
            GameController.instance.invController.SelectInventoryItem(true, GameController.instance.invController.inventorySlotArrayIndex);
        }

        GameController.instance.invController.UpdateInventoryDisplay();
    }

    public override void OnItemRightClick()
    {

    }
}