﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuseMachineWindow : UISystem, ITimeUpdate
{
    GameController controller;
    public FuseMachineController fuseController;

    public Button[] punkButtons;
    public Image item;
    public SuperTextMesh remainingTime;

    // Start is called before the first frame update
    void Start()
    {
        controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        punkButtons[0].onClick.AddListener(() => { ButtonClick(0); });
        punkButtons[1].onClick.AddListener(() => { ButtonClick(1); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateDisplay()
    {
        FuseMachine machine = fuseController.GetFuseMachineEntity();

        if (machine.punk_a != null)
        {
            punkButtons[0].gameObject.SetActive(true);
            
            punkButtons[0].image.sprite = PunkSystem.UpdatePunkSprite(machine.punk_a);
            punkButtons[0].image.material = PunkSystem.UpdatePunkMaterial(punkButtons[0].image.material, machine.punk_a);
        } else
        {
            punkButtons[0].gameObject.SetActive(false);
        }
        
        if (machine.punk_b != null)
        {
            punkButtons[1].gameObject.SetActive(true);

            punkButtons[1].image.sprite = PunkSystem.UpdatePunkSprite(machine.punk_b);
            punkButtons[1].image.material = PunkSystem.UpdatePunkMaterial(punkButtons[0].image.material, machine.punk_b);
        } else
        {
            punkButtons[1].gameObject.SetActive(false);
        }

        if (machine.catalyst != null)
        {
            item.gameObject.SetActive(true);
            item.sprite = Resources.Load<Sprite>("Items/" + machine.catalyst.item.id);
        } else
        {
            item.gameObject.SetActive(false);
        }

        remainingTime.text = machine.remainingTime == -1? "--" : machine.remainingTime.ToString() + "m";
    }

    public void OnTimeUpdate()
    {
        FuseMachine machine = fuseController.GetFuseMachineEntity();

        remainingTime.text = machine.remainingTime.ToString() + "m";
    }

    public void ButtonClick(int index)
    {
        if (index == 0 && fuseController.GetFuseMachineEntity().punk_a != null)
        {
            controller.UpdatePunkWindow(fuseController.GetFuseMachineEntity().punk_a);
        } else if (index == 1 && fuseController.GetFuseMachineEntity().punk_b != null)
        {
            controller.UpdatePunkWindow(fuseController.GetFuseMachineEntity().punk_b);
        }
    }
}
