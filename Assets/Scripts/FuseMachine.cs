﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class FuseMachine : Entity
{
    public Punk punk_a, punk_b;
    public ItemStack catalyst;
    public bool isFusing;
    public int remainingTime;

    public FuseMachine()
    {
        punk_a = null;
        punk_b = null;
        catalyst = null;
        isFusing = false;
        remainingTime = -1;

        entityName = "Fuse Machine";
    }

    public void SetTime()
    {
        // TO DO: Calculate time based on stats
        remainingTime = 20;
    }

    public bool AddPunk(Punk addedPunk)
    {
        if (punk_a == null)
        {
            punk_a = addedPunk;
            if (punk_a != null && punk_b != null)
            {
                SetTime();
            }
                  
            return true;
        } else if (punk_b == null)
        {
            punk_b = addedPunk;
            if (punk_a != null && punk_b != null)
            {
                SetTime();
            }

            return true;
        }

        return false;
    }

    public bool AddItem(ItemStack addedItem)
    {
        if (catalyst == null)
        {
            catalyst = addedItem;
            return true;
        } else if (catalyst != null && GameVariables.AddItem(catalyst))
        {
            catalyst = addedItem;
            return true;
        } else if (catalyst != null && !GameVariables.AddItem(catalyst))
        {
            return false;
        }

        return false;
    }

}